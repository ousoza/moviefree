@extends('layouts.app')
@section('content')


    <nav class="navbar navbar-dark bg-dark">
        <img src="https://image.flaticon.com/icons/svg/2916/2916399.svg" style="height: 50px; width: 50px;" class="img-thumbnail" >
        <a class="navbar-brand" style="color: white">Movie To Free</a>
        <form class="form-inline">
            <input class="form-control mr-sm-1 " type="search" placeholder="Search" aria-label="Search">
            <button  class="btn btn-light" type="submit">Search</button>
        </form>


        <form method="get" action="{{ url('/new') }}">
            <button class="btn btn-light  my-2 my-sm-0" type="submit">Create Movie</button>
        </form>

        <form method="get" action="{{ url('/') }}">
            <button  class="btn btn-light  my-2 my-sm-1" type="submit">Home</button>
        </form>
    </nav>


    <br>
    <h1 class="text-center" >Mange Data+++</h1>
    <br>

    <table class="table table-striped table-dark">
        <thead>
        <tr class="">
            <th scope="col">ID</th>
            <th scope="col">Movie</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Weathering With You ฤดูฝัน ฉันมีเธอ</td>
            <td><button type="button" class="btn btn-warning">edit</button></td>
            <td><button type="button" class="btn btn-danger">delete</button></td>


        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Sonic the Hedgehog โซนิค เดอะ เฮดจ์ฮ็อก</td>
            <td><button type="button" class="btn btn-warning">edit</button></td>
            <td><button type="button" class="btn btn-danger">delete</button></td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>The Incredibles รวมเหล่ายอดคนพิทักษ์โลก</td>
            <td><button type="button" class="btn btn-warning">edit</button></td>
            <td><button type="button" class="btn btn-danger">delete</button></td>
        </tr>

        <tr>
            <th scope="row">4</th>
            <td>Dragon Ball Super Broly ดราก้อนบอล ซูเปอร์ โบรลี่</td>
            <td><button type="button" class="btn btn-warning">edit</button></td>
            <td><button type="button" class="btn btn-danger">delete</button></td>
        </tr>

        <tr>
            <th scope="row">5</th>
            <td>My Hero Academia: Two Heroes กำเนิดใหม่ 2 วีรบุรุษ</td>
            <td><button type="button" class="btn btn-warning">edit</button></td>
            <td><button type="button" class="btn btn-danger">delete</button></td>
        </tr>

        <tr>
            <th scope="row">6</th>
            <td>Frozen 2 โฟรเซ่น 2 ผจญภัยปริศนาราชินีหิมะ</td>
            <td><button type="button" class="btn btn-warning">edit</button></td>
            <td><button type="button" class="btn btn-danger">delete</button></td>
        </tr>

        </tbody>
    </table>

@endsection
