@extends('layouts.app')
@section('content')


    <nav class="navbar navbar-dark bg-dark" >
        <img src="https://image.flaticon.com/icons/svg/2916/2916399.svg" style="height: 50px; width: 50px;" class="img-thumbnail" >
        <a class="navbar-brand" style="color: white">Movie To Free</a>
        <form class="form-inline">
            <input class="form-control mr-sm-1 " type="search" placeholder="Search" aria-label="Search">
            <button  class="btn btn-light" type="submit">Search</button>
        </form>


            <form method="get" action="{{ url('/new') }}">
                <button class="btn btn-light  my-2 my-sm-0" type="submit">Create Movie</button>
            </form>

            <form method="get" action="{{ url('/data') }}">
                <button  class="btn btn-light  my-2 my-sm-1" type="submit">Mange Data</button>
            </form>
    </nav>

    <div class="album py-5 bg-light" >
        <div class="container">
            <div class="row">
                <div class="col-md-4">

                    <h6> Weathering With You ฤดูฝัน ฉันมีเธอ</h6>
                    <div class="card mb-4 box-shadow">
                        <img src="https://www.movie-th.com/wp-content/uploads/2020/05/Weathering-With-You-213x300.jpg" alt="..." class="img-thumbnail"  >
                        <h6> view : 17 </h6>
                    </div>

                </div>

                <div class="col-md-4">
                    <h6> Sonic the Hedgehog โซนิค เดอะ เฮดจ์ฮ็อก </h6>
                    <div class="card mb-4 box-shadow">
                        <img src="https://www.movie-th.com/wp-content/uploads/2020/02/sonic-203x300.jpg" alt="..." class="img-thumbnail"  >
                        <h6> view </h6>
                    </div>
                </div>

                <div class="col-md-4">
                    <h6> movie3</h6>
                    <div class="card mb-4 box-shadow">
                        <img src="https://www.movie-th.com/wp-content/uploads/2020/03/The-Incredibles-203x300.jpg" alt="..." class="img-thumbnail"  >
                        <h6> view </h6>
                    </div>
                </div>

                <div class="col-md-4">
                    <h6> Dragon Ball Super Broly ดราก้อนบอล ซูเปอร์ โบรลี่ </h6>
                    <div class="card mb-4 box-shadow">
                        <img src="https://www.movie-th.com/wp-content/uploads/2020/03/Dragon-Ball-Super-Broly-200x300.jpg" alt="..." class="img-thumbnail"  >
                        <h6> view </h6>
                    </div>
                </div>

                <div class="col-md-4">
                    <h6>My Hero Academia: Two Heroes กำเนิดใหม่ 2 วีรบุรุษ </h6>
                    <div class="card mb-4 box-shadow">
                        <img src="https://www.movie-th.com/wp-content/uploads/2020/03/My-Hero-Academia-Two-Heroes-225x300.jpg" alt="..." class="img-thumbnail"  >
                        <h6> view </h6>
                    </div>
                </div>

                <div class="col-md-4">
                    <h6> Frozen 2 โฟรเซ่น 2   ผจญภัยปริศนาราชินีหิมะ </h6>
                    <div class="card mb-4 box-shadow">
                        <img src=" https://www.movie-th.com/wp-content/uploads/2020/02/Frozen-2-187x300.jpg" alt="..." class="img-thumbnail"  >
                        <h6> view </h6>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <br>
    <br>
    <h1 style="text-align:center; font-family: 'Prompt SemiBold';">SHORTEN URL WEBSITE</h1>

    <form method="get" action="{{ url('/new') }}">
        <button type="submit"   class="btn btn-outline-danger p-2" style="float: right;">Creat ShortUrl</button>
    </form>

    <br>
    <br>
    <br>


    <table class="table table-success">
        <thead class="thead">
        <tr>

            <th scope="col" style=" font-family: 'Prompt SemiBold';">CREATED AT</th>
            <th scope="col" style=" font-family: 'Prompt SemiBold';">LONG URL</th>
            <th scope="col" style=" font-family: 'Prompt SemiBold';">SHORT URL</th>
            <th scope="col" style=" font-family: 'Prompt SemiBold';"></th>
            <th scope="col" style=" font-family: 'Prompt SemiBold';">VIEW</th>

        </tr>
        </thead>

        @if(count($shorts)>0)
            @foreach($shorts as $short)
                <tbody>
                <tr class="table-light">

                    <td style=" font-family: 'Prompt SemiBold';">{{$short->created_at}}</td>
                    <td style=" font-family: 'Prompt SemiBold';"> <a href="{{ url($short->long_url) }}"> {{$short->long_url}}
                        </a>
                    </td>
                    <td style=" font-family: 'Prompt SemiBold';"> <input type="text" name="long_url" id="shortUrl{{$short->id}}" class="form-control col-12" value= " http://www.short172.local/t/{{$short->short_url}}">   </td>
                    <td style=" font-family: 'Prompt SemiBold';"><button type="submit" onclick="copy(this)" value="{{$short->id}}" id="copyBtn" class="btn btn-outline-info" style="float: left;">COPY</button></td>
                    <td class="text-center" style=" font-family: 'Prompt SemiBold';">{{$short->view}}</td>

                </tr>
                </tbody>
            @endforeach
        @endif
    </table>





    <script>
        function copy(clickCopy) {


            var id = clickCopy.value;
            var copyText = document.querySelector("#shortUrl"+id);
            copyText.select();
            copyText.setSelectionRange(0, 99999);
            document.execCommand("copy");
            alert("Copied the text: " + copyText.value);
        }


    </script>


@endsection
