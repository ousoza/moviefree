@extends('layouts.app')
@section('content')

    <nav class="navbar navbar-dark bg-dark">
        <img src="https://image.flaticon.com/icons/svg/2916/2916399.svg" style="height: 50px; width: 50px;" class="img-thumbnail" >
        <a class="navbar-brand" style="color: white">Movie To Free</a>
        <form class="form-inline">
            <input class="form-control mr-sm-1 " type="search" placeholder="Search" aria-label="Search">
            <button  class="btn btn-light" type="submit">Search</button>
        </form>


        <form method="get" action="{{ url('/') }}">
            <button class="btn btn-light  my-2 my-sm-0" type="submit">Home</button>
        </form>

        <form method="get" action="{{ url('/data') }}">
            <button  class="btn btn-light  my-2 my-sm-1" type="submit">Mange Data</button>
        </form>
    </nav>


    <br>
    <h1 class="text-center" >Create Movie</h1>
    <br>


    <div class="album py-5 bg-light bg-dark" >
        <div class="container">
            <form>
                <div class="form-group">
                    <label for="formGroupExampleInput" style="color: white"> Movie Name</label>
                    <input type="text" name="moviename" placeholder="Movie name" class="form-control">
                </div>

                <h6 style="color: white"> Movie file</h6>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>

                <br>
                <br>

                <h6 style="color: white"> Image file</h6>
                <div class="custom-file">
                    <input type="file" name="images" class="custom-file-input">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>

                <br>
                <br>

                <div class="form-group">
                    <label for="formGroupExampleInput2" style="color: white">Description</label>
                    <input type="text"  name="Description"  placeholder="Description" class="form-control">
                </div>

                <div class="row justify-content-md-center">
                    <button type="submit"  class="btn btn-primary col-6">CREATE MOVIE</button>
                </div>
            </form>
        </div>
    </div>









    <br>

    <form method="post" action="{{ url('/') }}">
        @csrf
        <br>
        <h1 >Create new ShortURL</h1>
        <br>
        <div class="container p-5" >

            <div class="row justify-content-md-center">
                {{--            <input type="text" name="title" class="form-control" value="{{ old('title') }}">--}}
                <div class="col-10">
                    <input type="text" name="long_url" class="form-control" placeholder="PASTE LONG URL">
                </div>
                <br>
                <br>

                <button type="submit" class="btn btn-primary col-6">CREATE SHORT URL</button>

            </div>

        </div>
    </form>
    <br>


@endsection
